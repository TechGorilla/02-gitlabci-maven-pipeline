# GitLab CI : Simple Java Maven Build 
---

The build is composed of  3 stages:
- Dependency check (Two parallel jobs)
- Test
- Install

![](images/pipeline.jpg)